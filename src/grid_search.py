import itertools
import subprocess

hyper1 = [10, 20, 30, 40]
for hy1 in itertools.product(hyper1):
    subprocess.run(
        ["dvc", "exp", "run", "--queue", "-S", f"mlp.max_iter={hy1[0]}"]
    )
