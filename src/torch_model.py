import mlflow
import mlflow.pytorch
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
import yaml
import torchvision
import os

from mlflow import MlflowException

os.environ["MLFLOW_TRACKING_USERNAME"] = "raghava"
os.environ["MLFLOW_TRACKING_PASSWORD"] = "raghava"
os.environ[
    "MLFLOW_TRACKING_URI"] = "https://mlflow.guck-tools.intranet.mpl.mpg.de"

experiment_name = "test torch model training with cml and mlflow"
run_name = "test run 1"

# Hyperparameters
batch_size = 8
epochs = 15
learning_rate = 0.001

# Start MLflow experiment
try:
    mlflow.create_experiment(experiment_name)
except MlflowException as e:
    print(e)
mlflow.set_experiment(experiment_name)

# Check if CUDA (GPU) is available
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"Using device: {device}")


# Define a simple CNN model
class SimpleCNN(nn.Module):
    def __init__(self):
        super(SimpleCNN, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, 3, 1)
        self.conv2 = nn.Conv2d(32, 64, 3, 1)
        self.fc1 = nn.Linear(64 * 5 * 5, 128)  # Update the input size here
        self.fc2 = nn.Linear(128, 10)
        self.relu = nn.ReLU()
        self.maxpool = nn.MaxPool2d(2)

    def forward(self, x):
        x = self.relu(self.conv1(x))
        x = self.maxpool(x)
        x = self.relu(self.conv2(x))
        x = self.maxpool(x)
        x = x.view(-1,
                   64 * 5 * 5)  # Reshape the tensor before fully connected layers
        x = self.relu(self.fc1(x))
        x = self.fc2(x)
        return x


# Load MNIST dataset
transform = transforms.Compose(
    [transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,))])
train_dataset = datasets.MNIST(root='./data', train=True, transform=transform,
                               download=True)
test_dataset = datasets.MNIST(root='./data', train=False, transform=transform)

train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size,
                                           shuffle=True)
test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size,
                                          shuffle=False)

# Initialize model, loss, and optimizer
model = SimpleCNN().to(device)
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

with mlflow.start_run(run_name=run_name):
    # Training loop
    for epoch in range(epochs):
        running_loss = 0.0
        for i, data in enumerate(train_loader, 0):
            inputs, labels = data[0].to(device), data[1].to(device)
            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()

        print(f"Epochs [{epoch + 1} / {epochs}]:: Train Loss: "
              f"{running_loss / len(train_loader)}")
        # Log metrics after each epoch
        mlflow.log_metric('training_loss', running_loss / len(train_loader),
                          step=epoch)

    # Log model and evaluate on test set
    mlflow.pytorch.log_model(model, "models")

    correct = 0
    total = 0
    with torch.no_grad():
        for data in test_loader:
            images, labels = data[0].to(device), data[1].to(device)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    accuracy = 100 * correct / total
    mlflow.log_metric('test_accuracy', accuracy)


    # Log predictions as artifacts
    def imshow(img):
        img = img / 2 + 0.5  # unnormalize
        npimg = img.numpy()
        plt.imshow(np.transpose(npimg, (1, 2, 0)))
        plt.axis('off')
        plt.savefig('predictions.png')


    # dataiter = iter(test_loader)
    images, labels = next(iter(test_loader))
    images, labels = images.to(device), labels.to(device)
    outputs = model(images)
    _, predicted = torch.max(outputs, 1)

    # Plot and log predictions
    fig = plt.figure(figsize=(10, 4))
    imshow(torchvision.utils.make_grid(images[:4].cpu()))
    plt.title(
        'Predicted: ' + ' '.join('%5s' % predicted[j].item() for j in range(4)))
    plt.tight_layout()
    plt.axis('off')
    plt.savefig("predictions.png")
    mlflow.log_artifact('predictions.png', 'evaluation_plots')
