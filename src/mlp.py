import os
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
import yaml
import mlflow
from mlflow import MlflowException
import numpy as np


os.environ["MLFLOW_TRACKING_USERNAME"] = "raghava"

os.environ["MLFLOW_TRACKING_PASSWORD"] = "raghava"

os.environ[
    "MLFLOW_TRACKING_URI"] = "https://mlflow.guck-tools.intranet.mpl.mpg.de"

train_data_path = "data/processed/Iris_train.csv"
model_path = 'models/mlp_model.pkl'

with open('params.yaml') as conf_file:
    config = yaml.safe_load(conf_file)

experiment_name = "test with cml runner"

try:
    mlflow.create_experiment(experiment_name)
except MlflowException as e:
    print(e)
mlflow.set_experiment(experiment_name)

# Create a small synthetic dataset
X, y = make_classification(n_samples=1000, n_features=20, n_classes=2,
                           random_state=42)

# Split data into train and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2,
                                                    random_state=42)

params = {
    'hidden_layer_sizes': (10,),
    'activation': 'relu',
    'solver': 'adam',
    'learning_rate': 'adaptive',
    'max_iter': 5
}

model = MLPClassifier(
    hidden_layer_sizes=params['hidden_layer_sizes'],
    activation=params['activation'],
    solver=params['solver'],
    learning_rate=params['learning_rate'],
    max_iter=params['max_iter']
)

with mlflow.start_run(
        run_name="test run 1",
        description="This model is created for the demo purpose!",
        log_system_metrics=True
):
    random_state = config['mlp']['random_state']

    for epoch in range(config['mlp']['max_iter']):
        model.partial_fit(X_train, y_train, classes=np.unique(y_train))
        # Evaluate on test set
        y_pred = model.predict(X_test)
        accuracy = accuracy_score(y_test, y_pred)
        loss = model.loss_

        # Log hyperparameters
        mlflow.log_params(params)

        # Log metrics
        mlflow.log_metric('accuracy', accuracy, step=epoch)
        mlflow.log_metric('loss', loss, step=epoch)

    # Log model
    mlflow.sklearn.log_model(model, "model")
