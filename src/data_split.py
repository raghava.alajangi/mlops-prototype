import pandas as pd
import yaml

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

feat_data_path = "data/featurize/featurize.csv"
train_data_path = "data/processed/Iris_train.csv"
test_data_path = "data/processed/Iris_test.csv"

with open('params.yaml') as conf_file:
    config = yaml.safe_load(conf_file)

feat_data = pd.read_csv(feat_data_path)

feat_data['Species'] = LabelEncoder().fit_transform(feat_data['Species'])

test_size = config['data_split']['test_size']
random_state = config['data_split']['random_state']

df_train, df_test = train_test_split(feat_data, test_size=test_size,
                                     random_state=random_state)

df_train.to_csv(train_data_path)
df_test.to_csv(test_data_path)
